# Great Stuff

Collection of stuff that are great and may be useful

## Disclaimer

This is not a curated list nor is it a `awesome-whatever`. Nor is it a list of
not so famous links.

This is just stuff I found on Github that either have great ideas, great code,
great functionality and so on.

## The List

### Web Apps
[fil](https://github.com/fatiherikli/fil) -
Playground for in-browser interpreters

### Apps
[zazu](https://github.com/tinytacoteam/zazu) -
Fully extensible and open source launcher for hackers, creators and dabblers

[Mutate](https://github.com/qdore/Mutate) -
Simple launcher inspired by Alfred for ubuntu and Fedora

[Left](http://wiki.xxiivv.com/Left) -
Left is a distractionless writing application

[vtop](https://github.com/MrRio/vtop) -
Wow such top. So stats. More better than regular top

### Extensions
[comment-vscode](https://github.com/pmkary/comment-vscode) -
KFCS plugin for Visual Studio Code

[vscode-gitlens](https://github.com/eamodio/vscode-gitlens) -
Supercharge Visual Studio Code's Git capabilities

#### Browsers
[markdown-here](https://github.com/adam-p/markdown-here) -
Extension that lets you write email in Markdown and render it before sending

### Development
[xo](https://github.com/sindresorhus/xo) -
Opinionated but configurable ESLint wrapper with lots of goodies included

[awesome-typescript-loader](https://github.com/s-panferov/awesome-typescript-loader) -
Awesome typescript loader for webpack

[redux-persist](https://github.com/rt2zz/redux-persist) -
Persist and rehydrate redux store

[redux-logger](https://github.com/evgenyrodionov/redux-logger) -
Logger for redux

[glamorous](https://github.com/paypal/glamorous) -
Maintainable CSS with React

[react-modal](https://github.com/reactjs/react-modal) -
Accessible modal dialog component for React

[query-string](https://github.com/sindresorhus/query-string) -
Parse and stringify URL query strings

[moment-jalali](https://github.com/jalaali/moment-jalaali) -
Jalaali (Jalali, Persian, Khorshidi, Shamsi) calendar system plugin for [moment.js](https://github.com/moment/moment)

[page.js](https://github.com/visionmedia/page.js) -
Micro client-side router inspired by the Express router

[blessed](https://github.com/chjj/blessed) -
High-level terminal interface library for node.js

[Inquirer.js](https://github.com/SBoudrias/Inquirer.js) -
Collection of common interactive command line user interfaces

[prompt](https://github.com/flatiron/prompt) -
Beautiful command-line prompt for node.js

[stdio](https://github.com/sgmonda/stdio) -
Standard input/output management module for NodeJS

[JSVerbalExpressions](https://github.com/VerbalExpressions/JSVerbalExpressions) -
JavaScript Regular expressions made easy

#### Tools
[nuup](https://github.com/pablopunk/nuup) -
Yet another `npm publish` with superpowers

[wuzz](https://github.com/asciimoo/wuzz) -
Interactive cli tool for HTTP inspection

[rtlcss](https://github.com/MohammadYounes/rtlcss) -
Framework for transforming CSS from LTR to RTL

[nexe](https://github.com/nexe/nexe) -
Create a single executable out of your node.js apps

#### Testing
[jest](https://github.com/facebook/jest) -
Delightful JavaScript Testing

[ava](https://github.com/avajs/ava) -
Futuristic test runner

[cypress](https://github.com/cypress-io/cypress) -
Fast, easy and reliable testing for anything that runs in a browser

#### Boilerplates and scaffoldings
[electron-react-typescript-boilerplate](https://github.com/iRath96/electron-react-typescript-boilerplate) -
Live editing development on desktop app

[lass](https://github.com/lassjs/lass) -
Lass scaffolds a modern package boilerplate for Node.js

#### UI
[sweetalert2](https://github.com/sweetalert2/sweetalert2) -
Beautiful, responsive, customizable, accessible JavaScript popup

### Utils
[godpaste](https://github.com/erbesharat/godpaste) -
Command line tool for creating items on dpaste

[express-apps](https://github.com/electron/electron-apps) -
Collection of apps built on Electron

[engineering-blogs](https://github.com/kilimchoi/engineering-blogs) -
Curated list of engineering blogs

### Curated Lists
[offline-first](https://github.com/pazguille/offline-first) -
Everything you need to know to create offline-first web apps

### Tutorials
[super-tiny-compiler](https://github.com/thejameskyle/the-super-tiny-compiler) -
Possibly the smallest compiler ever

[bash-handbook](https://github.com/denysdovhan/bash-handbook) -
For those who wanna learn Bash

### I don't know what to call it!
[coding-interview-university](https://github.com/jwasham/coding-interview-university) -
Complete computer science study plan to become a software engineer
